package com.anagaf.newsreader.ui

import android.app.Application
import com.anagaf.newsreader.RxTrampolineRule
import com.anagaf.newsreader.data.model.Article
import com.anagaf.newsreader.data.model.News
import com.anagaf.newsreader.data.repository.TestRepository
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.Exception

/**
 * Using Junit4 here because Junit5 is not compatible with Robolectic.
 *
 * Using default application class to avoid messing with Koin.
 */
@RunWith(RobolectricTestRunner::class)
@Config(application = Application::class)
internal class MainActivityModelTest {

    @Rule
    @JvmField
    val rxTrampolineRule = RxTrampolineRule()

    private var notificationHelper: TestNotificationHelper = TestNotificationHelper()

    private var newsRepository: TestRepository = TestRepository()

    private lateinit var model: MainActivityModel

    private val articleAbc = Article("aaa", "bbb", "ccc")
    private val articleXyz = Article("xxx", "yyy", "zzz")

    @Before
    internal fun setUp() {
        notificationHelper.reset()
        newsRepository.reset()
        model = MainActivityModel(notificationHelper, newsRepository)
    }

    @Test
    fun shouldHideNotificationOnStart() {
        notificationHelper.article = articleAbc
        model.onStart()
        assertNull(notificationHelper.article)
    }

    @Test
    fun shouldRetrieveNewsOnFirstStart() {
        model.onStart()
        assertTrue(newsRepository.newsRetrieved)
    }

    @Test
    fun shouldNotRetrieveNewsOnRepeatedStart() {
        newsRepository.news = News(listOf(articleAbc))
        model.onStart()
        newsRepository.reset()
        model.onStart()
        assertFalse(newsRepository.newsRetrieved)
    }

    @Test
    fun shouldShowNotificationOnStopIfArticleSelected() {
        val articleAbc = articleAbc
        newsRepository.news = News(listOf(articleAbc, articleXyz))
        model.onStart()
        model.onArticleSelected(1)
        model.onStop()
        assertEquals(articleXyz, notificationHelper.article)
    }

    @Test
    fun shouldNotShowNotificationOnStopIfArticleNotSelected() {
        newsRepository.news = News(listOf(articleAbc, articleXyz))
        model.onStart()
        model.onArticleSelected(-1)
        model.onStop()
        assertNull(notificationHelper.article)
    }

    @Test
    fun shouldSetInProgressWhileRetrievingNews() {
        newsRepository.news = News(listOf(articleAbc, articleXyz))
        var inProgressHasBeenEnabled = false
        model.inProgress.observeForever {
            if (it) inProgressHasBeenEnabled = true
        }
        model.retrieveNews()
        assertTrue(inProgressHasBeenEnabled)
        assertTrue(model.inProgress.value == false)
    }

    @Test
    fun shouldClearArticleSelectionBeforeRetrievingNews() {
        newsRepository.news = News(listOf(articleAbc, articleXyz))
        model.selectedArticlePosition = 1
        model.retrieveNews()
        assertTrue(model.selectedArticlePosition < 0)
    }

    @Test
    fun shouldShowNews() {
        newsRepository.news = News(listOf(articleAbc, articleXyz))
        model.retrieveNews()
        assertEquals(newsRepository.news, model.news.value)
        assertTrue(model.newsListVisible.value == true)
        assertTrue(model.noNewsMessageVisible.value == false)
    }

    @Test
    fun shouldShowNoNewsMessageIfNoNewsAvailable() {
        newsRepository.news = News(listOf())
        model.retrieveNews()
        assertTrue(model.newsListVisible.value == false)
        assertTrue(model.noNewsMessageVisible.value == true)
    }

    @Test
    fun shouldShowNoNewsMessageInCaseOfError() {
        newsRepository.exception = Exception("Test")
        model.retrieveNews()
        assertTrue(model.newsListVisible.value == false)
        assertTrue(model.noNewsMessageVisible.value == true)
    }
}