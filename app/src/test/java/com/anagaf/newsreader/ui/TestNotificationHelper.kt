package com.anagaf.newsreader.ui

import com.anagaf.newsreader.data.model.Article

class TestNotificationHelper : NotificationHelper {

    var article: Article? = null

    override fun hideNotification() {
        article = null
    }

    override fun showNotification(article: Article) {
        this.article = article
    }

    fun reset() {
        article = null
    }
}