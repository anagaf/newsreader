package com.anagaf.newsreader.data.repository

import com.anagaf.newsreader.data.model.News
import io.reactivex.Single

class TestRepository : NewsRepository {

    var newsRetrieved: Boolean = false

    var exception: Throwable? = null

    var news: News = News()

    override fun getNews(): Single<News> {
        newsRetrieved = true
        if (exception != null) {
            return Single.error(exception)
        }
        return Single.just(news)
    }

    fun reset() {
        newsRetrieved = false
        exception = null
        news = News()
    }
}