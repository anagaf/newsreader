package com.anagaf.newsreader.data.repository.lenta

import com.fasterxml.jackson.databind.ObjectMapper
import com.anagaf.newsreader.data.model.Article
import com.anagaf.newsreader.data.model.News
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class LentaNewsDeserializerTest {

    @Test
    fun shouldDeserializeValidResponse() {
        val news = getNews("lenta_news_response.json")

        assertEquals(100, news.size)
        assertTrue(
            news.contains(
                Article(
                    "Названы условия приезда российских олигархов  в Давос",
                    "Список состоит из пяти пунктов",
                    "https://icdn.lenta.ru/images/2018/12/17/13/20181217131322975/detail_8671affb135a73631d86b7f442f25965.jpg"
                )
            )
        )
        assertTrue(
            news.contains(
                Article(
                    "Аршавин вспомнил о полезности Кокорина и Мамаева для общества",
                    null,
                    "https://icdn.lenta.ru/images/2018/12/17/11/20181217111339490/detail_8236efdfa2dc0833b5a740c2fad5c9db.jpg"
                )
            )
        )
        assertTrue(
            news.contains(
                Article(
                    "Объявлены результаты обработки половины протоколов голосования в Приморье",
                    "На первом месте идет врио главы региона Олег Кожемяко",
                    null
                )
            )
        )
    }

    @Test
    fun shouldFailIfNotArticleTitleSet() {
        assertThrows(Throwable::class.java) { getNews("lenta_news_response_no_title.json") }
    }

    @Test
    fun shouldFailIfEmptyArticleTitleSet() {
        assertThrows(Throwable::class.java) { getNews("lenta_news_response_empty_title.json") }
    }

    private fun getNews(fileName: String): News {
        val stream = Thread.currentThread()
            .contextClassLoader
            .getResourceAsStream(fileName)
        val mapper = ObjectMapper()
        val deserializer = LentaNewsDeserializer()
        return deserializer.deserialize(
            mapper.factory.createParser(stream),
            mapper.deserializationContext
        )
    }
}