package com.anagaf.newsreader.data.repository.database

import com.anagaf.newsreader.data.database.NewsDao
import com.anagaf.newsreader.data.model.Article

class TestNewsDao : NewsDao {

    var articles: List<Article> = listOf()

    override fun getAll(): List<Article> {
        return articles
    }

    override fun insertAll(articles: List<Article>) {
        this.articles = articles
    }
}