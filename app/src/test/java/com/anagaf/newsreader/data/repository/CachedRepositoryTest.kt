package com.anagaf.newsreader.data.repository

import com.anagaf.newsreader.data.model.Article
import com.anagaf.newsreader.data.model.News
import com.anagaf.newsreader.data.repository.database.TestNewsDao
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class CachedRepositoryTest {

    private val remoteRepository: TestRepository = TestRepository()

    private val newsDao: TestNewsDao = TestNewsDao()

    private lateinit var cachedRepository: NewsRepository

    @BeforeEach
    internal fun setUp() {
        cachedRepository = CachedNewsRepository(remoteRepository, newsDao)
    }

    @Test
    @DisplayName("should return news from repository and write them to cache")
    fun shouldReturnNewsFromRemoteRepository() {
        val remoteNews = News(listOf(Article("aaa", "bbb", "ccc")))
        remoteRepository.news = remoteNews
        val cachedArticles = listOf(Article("xxx", "yyy", "zzz"))
        newsDao.articles = cachedArticles
        cachedRepository.getNews().test().assertValue(remoteNews)
        assertEquals(remoteNews, News(newsDao.articles))
    }

    @Test
    @DisplayName("should return news from cache if repository request fails")
    fun shouldReturnNewsFromCache() {
        remoteRepository.exception = Exception("Test")
        val cachedArticles = listOf(Article("xxx", "yyy", "zzz"))
        newsDao.articles = cachedArticles
        cachedRepository.getNews().test().assertValue(News(cachedArticles))
    }
}