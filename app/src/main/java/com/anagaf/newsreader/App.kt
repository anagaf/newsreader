package com.anagaf.newsreader

import android.app.Application
import androidx.room.Room
import com.anagaf.newsreader.data.database.DATABASE_FILENAME
import com.anagaf.newsreader.data.database.NewsDatabase
import com.anagaf.newsreader.data.repository.CachedNewsRepository
import com.anagaf.newsreader.data.repository.lenta.LentaNewsRepository
import com.anagaf.newsreader.ui.NotificationHelperImpl
import com.anagaf.newsreader.ui.MainActivityModel
import org.koin.android.ext.android.startKoin
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.ext.koin.viewModel

import org.koin.dsl.module.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        val appModule = module {
            viewModel {
                val remoteRepository = LentaNewsRepository()

                val database = Room.databaseBuilder(
                    this.androidApplication(),
                    NewsDatabase::class.java,
                    DATABASE_FILENAME
                ).build()

                val cachedRepository = CachedNewsRepository(remoteRepository, database.getNewsDao())

                val notifier = NotificationHelperImpl(this@App)

                MainActivityModel(notifier, cachedRepository)
            }
        }

        startKoin(this, listOf(appModule))
    }
}