package com.anagaf.newsreader.data.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "news")
data class Article(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val title: String,
    val text:String?,
    val imageUrl:String?) {

    @Ignore
    constructor(title: String, text:String?, imageUrl: String?) : this(0, title, text, imageUrl)
}