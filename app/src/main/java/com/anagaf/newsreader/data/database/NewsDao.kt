package com.anagaf.newsreader.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.anagaf.newsreader.data.model.Article

@Dao
interface NewsDao {

    @Query("SELECT * FROM news")
    fun getAll(): List<Article>

    @Insert
    fun insertAll(articles: List<Article>)
}