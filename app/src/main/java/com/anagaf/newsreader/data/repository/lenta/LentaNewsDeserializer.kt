package com.anagaf.newsreader.data.repository.lenta

import com.anagaf.newsreader.data.model.Article
import com.anagaf.newsreader.data.model.News
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode

/**
 * Deserializer that parses lenta.ru API response.
 */
class LentaNewsDeserializer : JsonDeserializer<News>() {

    override fun deserialize(parser: JsonParser?, context: DeserializationContext?): News {
        val rootNode: JsonNode = parser?.codec?.readTree(parser)
            ?: throw JsonParseException(parser, "Unexpected response format: cannot access root node")
        val headlinesNode = rootNode["headlines"]
        val articles = ArrayList<Article>()
        for (i in 0 until headlinesNode.size()) {
            val article = parseArticle(headlinesNode[i])
            if (article != null) {
                articles += article
            } else {
                throw JsonParseException(parser, "Cannot parse article #$i")
            }

        }
        return News(articles)
    }

    private fun parseArticle(node: JsonNode): Article? {
        val infoNode = node.get("info") ?: return null
        val title = infoNode.get("title")?.asText()
        if (title.isNullOrEmpty()) {
            return null
        }
        return Article(
            title,
            infoNode.get("rightcol")?.asText(),
            node.get("title_image")?.get("url")?.asText()
        )
    }
}