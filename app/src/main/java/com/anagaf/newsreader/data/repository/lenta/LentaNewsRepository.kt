package com.anagaf.newsreader.data.repository.lenta

import com.anagaf.newsreader.data.model.News
import com.anagaf.newsreader.data.repository.NewsRepository
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

/**
 * News repository implementation that retrieves news from lenta.ru API.
 */
class LentaNewsRepository : NewsRepository {

    private val retrofitService: LentaRetrofitService by lazy {
        val objectMapper = ObjectMapper()

        val objectMapperModule = SimpleModule()
        objectMapperModule.addDeserializer(News::class.java, LentaNewsDeserializer())
        objectMapper.registerModule(objectMapperModule)

        val httpClient = OkHttpClient.Builder().build()

        Retrofit.Builder()
            .client(httpClient)
            .baseUrl("https://api.lenta.ru")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .build()
            .create(LentaRetrofitService::class.java)
    }


    override fun getNews(): Single<News> {
        return retrofitService.getNews()
    }
}