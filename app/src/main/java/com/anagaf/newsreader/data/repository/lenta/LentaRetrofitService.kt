package com.anagaf.newsreader.data.repository.lenta

import com.anagaf.newsreader.data.model.News
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Retrofit service for lenta.ru API.
 */
interface LentaRetrofitService {

    /**
     * Returns latest news.
     */
    @GET("lists/latest")
    fun getNews(): Single<News>
}