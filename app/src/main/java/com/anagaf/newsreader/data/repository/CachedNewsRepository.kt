package com.anagaf.newsreader.data.repository

import com.anagaf.newsreader.data.database.NewsDao
import com.anagaf.newsreader.data.model.News
import io.reactivex.Single

/**
 * News repository implementation that reads news from the cache if the remote repository cannot provide them.
 */
class CachedNewsRepository(
    private val remoteRepository: NewsRepository,
    private val cache: NewsDao
) : NewsRepository {

    override fun getNews(): Single<News> {
        return remoteRepository.getNews()
            .doAfterSuccess {
                cache.insertAll(it)
            }
            .onErrorReturn {
                News(cache.getAll())
            }
    }
}