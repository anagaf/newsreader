package com.anagaf.newsreader.data.repository

import com.anagaf.newsreader.data.model.News
import io.reactivex.Single

/**
 * News repository.
 */
interface NewsRepository {

    /**
     * Returns news.
     */
    fun getNews(): Single<News>
}