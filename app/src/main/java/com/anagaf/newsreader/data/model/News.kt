package com.anagaf.newsreader.data.model

data class News(private val articles: List<Article>) : List<Article> by articles {
    constructor() : this(listOf())
}