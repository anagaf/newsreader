package com.anagaf.newsreader.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.anagaf.newsreader.data.model.Article

const val DATABASE_FILENAME = "db.db"

@Database(entities = [Article::class], version = 1, exportSchema = false)
abstract class NewsDatabase : RoomDatabase() {
    abstract fun getNewsDao(): NewsDao
}