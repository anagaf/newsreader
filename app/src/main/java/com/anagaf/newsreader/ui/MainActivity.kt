package com.anagaf.newsreader.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.anagaf.newsreader.R
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Main activity.
 */
class MainActivity : AppCompatActivity() {

    private val vm: MainActivityModel by viewModel()

    private lateinit var layoutManager: NewsLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        swipeRefreshLayout.setOnRefreshListener {
            vm.retrieveNews()
        }

        layoutManager = NewsLayoutManager(this)

        list.layoutManager = layoutManager
        list.adapter = NewsAdapter(vm::onArticleSelected)
        list.addItemDecoration(NewsItemDecoration())

        vm.news.observe(this, Observer { news ->
            (list.adapter as NewsAdapter).setContent(news, vm.selectedArticlePosition)
        })

        vm.inProgress.observe(this, Observer { inProgress ->
            swipeRefreshLayout.isRefreshing = inProgress
        })

        vm.newsListVisible.observe(this, Observer { visible ->
            list.visibility = if (visible) VISIBLE else GONE
        })

        vm.noNewsMessageVisible.observe(this, Observer { visible ->
            noNewsMessage.visibility = if (visible) VISIBLE else GONE
        })
    }

    override fun onStart() {
        super.onStart()
        vm.onStart()
    }

    override fun onStop() {
        vm.onStop()
        super.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_refresh -> {
                vm.retrieveNews()
                true
            }
            else -> false
        }
    }
}
