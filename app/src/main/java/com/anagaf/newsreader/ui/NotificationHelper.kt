package com.anagaf.newsreader.ui

import com.anagaf.newsreader.data.model.Article

/**
 * Notification helper - shows and hides notifications.
 */
interface NotificationHelper {

    /**
     * Hides notification.
     */
    fun hideNotification()

    /**
     * Shows notification with article content.
     *
     * @param article article
     */
    fun showNotification(article: Article)
}