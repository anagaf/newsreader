package com.anagaf.newsreader.ui

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.core.app.NotificationCompat
import com.anagaf.newsreader.R
import com.anagaf.newsreader.data.model.Article
import com.squareup.picasso.Picasso

private const val channelId = "channelId"
private const val notificationId = 1

/**
 * Default notification helper implementation.
 */
class NotificationHelperImpl(private val appContext: Context) : NotificationHelper {

    override fun hideNotification() {
        val notificationManager: NotificationManager =
            appContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(notificationId)
    }

    override fun showNotification(article: Article) {
        if (article.imageUrl != null) {
            Picasso.get()
                .load(article.imageUrl)
                .into(object : com.squareup.picasso.Target {
                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                    }

                    override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
                    }

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        showNotification(article, bitmap)
                    }
                })
        }
    }

    private fun showNotification(article: Article, image: Bitmap?) {
        val notificationManager: NotificationManager =
            appContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = appContext.getString(R.string.channel_name)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, name, importance).apply {
            }
            notificationManager.createNotificationChannel(channel)
        }

        val intent = Intent(appContext, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        val pendingIntent = PendingIntent.getActivity(appContext, 0, intent, 0)

        val notification = NotificationCompat.Builder(appContext, channelId)
            .setSmallIcon(R.drawable.ic_notification)
            .setContentTitle(article.title)
            .setLargeIcon(image)
            .setStyle(NotificationCompat.BigTextStyle().bigText(article.text))
            .setContentIntent(pendingIntent)
            .build()

        notificationManager.notify(notificationId, notification)
    }
}