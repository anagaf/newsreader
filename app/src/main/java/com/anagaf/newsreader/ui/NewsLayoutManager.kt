package com.anagaf.newsreader.ui

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * News RecyclerView layout manager that scales item according to their on screen position.
 */
class NewsLayoutManager(context: Context?) : LinearLayoutManager(context) {

    // scale ratio (items on the top and bottom of the screen are 80% smalled than the central item)
    private val scaleRatio = 0.8f

    override fun scrollVerticallyBy(dy: Int, recycler: RecyclerView.Recycler?, state: RecyclerView.State?): Int {
        val scrolled = super.scrollVerticallyBy(dy, recycler, state)
        scaleItems()
        return scrolled
    }

    override fun getChildAt(index: Int): View? {
        val view = super.getChildAt(index)
        if (view != null) {
            scaleView(view, height / 2f)
        }
        return view
    }

    private fun scaleItems() {
        val midpoint = height / 2f
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            if (child != null) {
                scaleView(child, midpoint)
            }
        }
    }

    private fun scaleView(view: View, midpoint: Float) {
        if (getDecoratedTop(view) < height) {
            val childMidpoint = (getDecoratedBottom(view) + getDecoratedTop(view)) / 2f
            val scale = 1f - (1 - scaleRatio) * Math.abs(midpoint - childMidpoint) / midpoint
            view.scaleX = scale
            view.scaleY = scale
        }
    }
}