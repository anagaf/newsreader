package com.anagaf.newsreader.ui

import android.content.Context
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.anagaf.newsreader.R

/**
 * News RecyclerView item.
 */
class ArticleView(context: Context) : CardView(context) {

    val titleView: TextView
    val textView: TextView
    val imageView: ImageView

    init {
        inflate(context, R.layout.view_article, this)

        setCardBackgroundColor(ContextCompat.getColorStateList(context, R.color.bkg_card))
        radius = resources.getDimension(R.dimen.corner_radius)

        layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)

        titleView = findViewById(R.id.article_title)
        textView = findViewById(R.id.article_text)
        imageView = findViewById(R.id.article_image)
    }
}