package com.anagaf.newsreader.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.anagaf.newsreader.data.model.News
import com.anagaf.newsreader.data.repository.NewsRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Main activity view model.
 */
class MainActivityModel(
    private val notificationHelper: NotificationHelper,
    private val newsRepository: NewsRepository
) : ViewModel() {

    val news = MutableLiveData<News>()
    val newsListVisible = MutableLiveData<Boolean>()
    val inProgress = MutableLiveData<Boolean>()
    val noNewsMessageVisible = MutableLiveData<Boolean>()

    var selectedArticlePosition: Int = -1

    private val disposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun onStart() {
        notificationHelper.hideNotification()
        if (news.value == null) {
            retrieveNews()
        }
    }

    fun onStop() {
        val news = this.news.value
        if (news != null && selectedArticlePosition in 0..news.lastIndex) {
            notificationHelper.showNotification(news[selectedArticlePosition])
        }
    }

    fun onArticleSelected(position: Int) {
        this.selectedArticlePosition = position
    }

    fun retrieveNews() {
        inProgress.value = true
        selectedArticlePosition = -1
        disposable.add(
            newsRepository
                .getNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onNewsAvailable, this::onError)
        )
    }

    private fun onNewsAvailable(it: News) {
        if (it.isEmpty()) {
            showNoNewsMessage()
        } else {
            news.value = it
            inProgress.value = false
            newsListVisible.value = true
            noNewsMessageVisible.value = false
        }
    }

    private fun onError(it: Throwable?) {
        showNoNewsMessage()
        news.value = News()
    }

    private fun showNoNewsMessage() {
        inProgress.value = false
        newsListVisible.value = false
        noNewsMessageVisible.value = true
    }
}