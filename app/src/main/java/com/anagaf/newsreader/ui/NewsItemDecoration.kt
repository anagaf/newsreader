package com.anagaf.newsreader.ui

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * News RecyclerView item decoration that adds margins around the item.
 */
class NewsItemDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.set(10, 10, 10, 10)
    }
}