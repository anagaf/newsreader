package com.anagaf.newsreader.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anagaf.newsreader.data.model.News
import com.squareup.picasso.Picasso

private const val SELECTION_CHANGE_PAYLOAD = 1

/**
 * News RecyclerView adapter.
 */
class NewsAdapter(
    private val selectionListener: (Int) -> Unit
) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    private var news = News()

    private var selectedArticlePosition: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ArticleView(parent.context)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return news.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        onBindViewHolder(holder, position, mutableListOf())
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        holder.view.isSelected = (position == selectedArticlePosition)
        if (payloads.isEmpty()) {
            // payloads are non-empty in case of selection change

            val article = news[position]


            holder.view.titleView.text = article.title

            holder.view.textView.text = article.text

            Picasso.get()
                .load(article.imageUrl)
                .into(holder.view.imageView)

            holder.view.setOnClickListener {
                onArticleClick(position)
            }
        }
    }

    fun setContent(news: News, selectedArticleIndex: Int) {
        this.news = news
        this.selectedArticlePosition = selectedArticleIndex
        notifyDataSetChanged()
    }

    private fun onArticleClick(position: Int) {
        val prevSelectedArticlePosition = selectedArticlePosition
        selectedArticlePosition = if (selectedArticlePosition == position) -1 else position
        if (isValidPosition(prevSelectedArticlePosition)) {
            //notifyItemChanged(prevSelectedArticlePosition)
            notifyItemChanged(prevSelectedArticlePosition, SELECTION_CHANGE_PAYLOAD)
        }
        if (isValidPosition(selectedArticlePosition)) {
            notifyItemChanged(selectedArticlePosition, SELECTION_CHANGE_PAYLOAD)
        }
        selectionListener(selectedArticlePosition)
    }

    private fun isValidPosition(position: Int): Boolean = (position in 0 until itemCount)

    class ViewHolder(val view: ArticleView) : RecyclerView.ViewHolder(view)
}